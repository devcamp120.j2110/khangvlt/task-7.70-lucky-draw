
import { Row, Label, Col, Button } from 'reactstrap'
function RandomTicket({
    displayRandom,
    arrRandom,
    setArr,
    clickDoSo
}) {
    const onClickQuaySo = () => {
        setArr()
        document.getElementById('btnQuaySo').disabled = true
        document.getElementById('btnDoSo').style = { display: 'block' }
    }
    const onClickDoSo = () => {
        clickDoSo()
    }
    return (
        <>
            <div id="divLuckyDraw" style={{ display: displayRandom }}>
                <Row>
                    <Col className="text-center">
                        <Label><h1>Lucky Draw</h1></Label>
                    </Col>
                </Row>
                <Row className="p-2">
                    {arrRandom.map((item, index) => (
                        <div key={index}>
                            <span className="dot"><b>{item[0]}</b></span>
                            <span className="dot"><b>{item[1]}</b></span>
                            <span className="dot"><b>{item[2]}</b></span>
                            <span className="dot"><b>{item[3]}</b></span>
                            <span className="dot"><b>{item[4]}</b></span>
                            <span className="dot"><b>{item[5]}</b></span>
                        </div>

                    ))}
                </Row>
                <Row className="p-2 text-center">
                    <Col></Col>
                    <Col xs='6'>
                        <Button onClick={onClickQuaySo} id='btnQuaySo' className="btn-success" style={{ marginRight: 15 }}>Quay số</Button>
                        <Button onClick={onClickDoSo} className="btn-warning" style={{ display: 'none' }} id="btnDoSo">Dò số</Button>
                    </Col>
                    <Col></Col>
                </Row>
            </div>
        </>
    )
}
export default RandomTicket