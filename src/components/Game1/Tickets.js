import {Row, Label} from 'reactstrap'
function Tickets({arrTicket}) {
    return (
        <>
            <Row className="text-center">
                <Label>Số bạn mua là</Label>
                {arrTicket.map((item, index) => (
                    <div key={index}>
                        <span>{item[0]} - </span>
                        <span>{item[1]} - </span>
                        <span>{item[2]} - </span>
                        <span>{item[3]} - </span>
                        <span>{item[4]} - </span>
                        <span>{item[5]}</span>
                    </div>
                ))}
            </Row>
        </>
    )
}
export default Tickets