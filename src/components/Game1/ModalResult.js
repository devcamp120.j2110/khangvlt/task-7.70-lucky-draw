
import { Button, Modal, ModalBody, ModalFooter, ModalHeader, Row } from "reactstrap"

function ModalResult({ open, setModal, countNumber, compareArr, randomArr, ticketArr }) {
    const changeHandle = () => {
        setModal(false)
    }
    const onClickPlayAgain = () => {
        window.location.reload()
    }
    return (
        <>
            <Modal id="modal" isOpen={open} toggle={changeHandle}>
                <ModalHeader><b>KẾT QUẢ LƯỢT CHƠI CỦA BẠN</b></ModalHeader>
                <ModalBody>
                    <div>
                        <pre>Số hệ thống :
                            {randomArr.map((item, index) => (
                                <p key={index}>{item[0]} - {item[1]} - {item[2]} - {item[3]} - {item[4]} - {item[5]}</p>
                            ))}
                        </pre>
                    </div>

                    <div>
                        <pre>Số bạn mua :
                            {ticketArr.map((item, index) => (
                                <p key={index}>{item[0]} - {item[1]} - {item[2]} - {item[3]} - {item[4]} - {item[5]}</p>
                            ))}
                        </pre>
                    </div>
                    <div>
                        <pre>Bạn đã trúng {countNumber} số là :
                            {compareArr.map((item, index) => (
                                <span key={index}> {item} </span>
                            ))}
                        </pre>
                    </div>
                </ModalBody>
                <ModalFooter><Button onClick={onClickPlayAgain} className="btn-success">Chơi lại</Button></ModalFooter>
            </Modal>
        </>
    )
}
export default ModalResult