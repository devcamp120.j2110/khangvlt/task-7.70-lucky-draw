import { Row, Col, Input, Button } from 'reactstrap'
import 'react-toastify/dist/ReactToastify.css';
import { toast, ToastContainer } from "react-toastify"
function BuyTicket({
    changeBuyBall1,
    changeBuyBall2,
    changeBuyBall3,
    changeBuyBall4,
    changeBuyBall5,
    changeBuyBall6,
    setArr,
    buyBack
}) {
    const onChangeBuyBall1 = (event) => {
        if (event.target.value > 45) {
            toast.error('Nhập lại')
        }
        else {
            changeBuyBall1(parseInt(event.target.value))
        }
    }
    const onChangeBuyBall2 = (event) => {
        if (event.target.value > 45) {
            toast.error('Nhập lại')
        }
        else {
            changeBuyBall2(parseInt(event.target.value))
        }
    }
    const onChangeBuyBall3 = (event) => {
        if (event.target.value > 45) {
            toast.error('Nhập lại')
        }
        else {
            changeBuyBall3(parseInt(event.target.value))
        }
    }
    const onChangeBuyBall4 = (event) => {
        if (event.target.value > 45) {
            toast.error('Nhập lại')
        }
        else {
            changeBuyBall4(parseInt(event.target.value))
        }
    }
    const onChangeBuyBall5 = (event) => {
        if (event.target.value > 45) {
            toast.error('Nhập lại')
        }
        else {
            changeBuyBall5(parseInt(event.target.value))
        }
    }
    const onChangeBuyBall6 = (event) => {
        if (event.target.value > 45) {
            toast.error('Nhập lại')
        }
        else {
            changeBuyBall6(parseInt(event.target.value))
        }
    }
    const onClickBuyBall = () => {
        setArr()
    }
    const onClickBuyBackBall = () => {
        buyBack()
    }
    return (
        <>
            <Row className="p-2 text-center">
                <Col><h1>CHỌN SỐ BẠN MUỐN MUA</h1></Col>
            </Row>
            <Row className="p-2" id="rowBuyBall" >
                <Col><Input onChange={onChangeBuyBall1} id='buyBall1' placeholder='Số 1' maxLength='2'></Input></Col>
                <Col><Input onChange={onChangeBuyBall2} id='buyBall2' placeholder='Số 2' maxLength='2'></Input></Col>
                <Col><Input onChange={onChangeBuyBall3} id='buyBall3' placeholder='Số 3' maxLength='2'></Input></Col>
                <Col><Input onChange={onChangeBuyBall4} id='buyBall4' placeholder='Số 4' maxLength='2'></Input></Col>
                <Col><Input onChange={onChangeBuyBall5} id='buyBall5' placeholder='Số 5' maxLength='2'></Input></Col>
                <Col><Input onChange={onChangeBuyBall6} id='buyBall6' placeholder='Số 6' maxLength='2'></Input></Col>
            </Row>
            <Row className="p-2">
                <Col></Col>
                <Col xs='6' className="text-center">
                    <Button onClick={onClickBuyBall} id='btnBuyBall' className="btn-danger" style={{ marginRight: 10 }}>Confirm</Button>
                    <Button onClick={onClickBuyBackBall} id="btnBuyBackBall" style={{ display: 'none' }} className="btn-warning">Mua lại</Button>
                </Col>
                <Col></Col>
            </Row>
            <ToastContainer />
        </>
    )
}
export default BuyTicket