import BuyTicket from "./BuyTicket"
import RandomTicket from "./RandomTicket"
import { useEffect, useState } from 'react'
import { toast } from "react-toastify"
import Tickets from "./Tickets"
import ModalResult from "./ModalResult"
function Game1() {
    // VÙNG BUY BALL
    const [buyball1, setbuyBall1] = useState()
    const [buyball2, setbuyBall2] = useState()
    const [buyball3, setbuyBall3] = useState()
    const [buyball4, setbuyBall4] = useState()
    const [buyball5, setbuyBall5] = useState()
    const [buyball6, setbuyBall6] = useState()
    const [arrBuyBall, setArrBuyBall] = useState([
    ])
    let tempArray = []

    const validateArrBuyBall = () => {
        let flag = 0;
        let size = tempArray.length
        for (let i = 0; i < size - 1; ++i) {
            for (let j = i + 1; j < size; ++j) {
                if (tempArray[i] == tempArray[j]) {
                    /*Tìm thấy 1 phần tử trùng là đủ và dừng vòng lặp*/
                    flag = 1;
                    break;
                }
            }
        }
        return flag
    }
    const pushArrBuyBall = () => {
        tempArray.push(buyball1, buyball2, buyball3, buyball4, buyball5, buyball6)
        let valiData = validateArrBuyBall()
        if (valiData === 1) {
            toast.error('Có số trùng nhau')
        }
        else {
            toast.success('Mua thành công')
            document.getElementById('buyBall1').disabled = true
            document.getElementById('buyBall2').disabled = true
            document.getElementById('buyBall3').disabled = true
            document.getElementById('buyBall4').disabled = true
            document.getElementById('buyBall5').disabled = true
            document.getElementById('buyBall6').disabled = true
            document.getElementById('btnBuyBall').disabled = true
            document.getElementById('btnBuyBackBall').style = { display: 'block' }
            document.getElementById('btnQuaySo').disabled = false
            setDisplay('block')
            setArrBuyBall([tempArray])
        }
    }
    const buyBackBall = () => {
        setArrBuyBall([])
        setArrRandomBall([])
        setDisplay('none')
        document.getElementById('buyBall1').disabled = false
        document.getElementById('buyBall2').disabled = false
        document.getElementById('buyBall3').disabled = false
        document.getElementById('buyBall4').disabled = false
        document.getElementById('buyBall5').disabled = false
        document.getElementById('buyBall6').disabled = false
        document.getElementById('btnBuyBall').disabled = false
    }
    // VÙNG RANDOM BALL
    const [display, setDisplay] = useState('none');
    const [arrRandomBall, setArrRandomBall] = useState([
    ])

    const checkNumber = (paramTempArray, paramNumber) => {
        while (paramTempArray.indexOf(paramNumber) !== -1) {
            paramNumber = parseInt(Math.random(1) * 45) + 1;
        }
        paramTempArray.push(paramNumber)
    }

    const pushArrRandomBall = () => {
        let tempArray = []
        let vBall1 = parseInt(Math.random(1) * 45) + 1;
        tempArray.push(vBall1);

        let vBall2 = parseInt(Math.random(1) * 45) + 1;
        checkNumber(tempArray, vBall2);

        let vBall3 = parseInt(Math.random(1) * 45) + 1;
        checkNumber(tempArray, vBall3);

        let vBall4 = parseInt(Math.random(1) * 45) + 1;
        checkNumber(tempArray, vBall4);

        let vBall5 = parseInt(Math.random(1) * 45) + 1;
        checkNumber(tempArray, vBall5);

        let vBall6 = parseInt(Math.random(1) * 45) + 1;
        checkNumber(tempArray, vBall6);

        setArrRandomBall([
            tempArray
        ])

    }

    // VÙNG DÒ SỐ
    const [count, setCount] = useState(0)
    const [compare, setCompare] = useState([])

    const compare2Arr = () => {
        let vcompare = arrRandomBall[0].filter((e) => arrBuyBall[0].includes(e))
        setCompare(vcompare)
        setCount(vcompare.length)
        setStateModal(true);
    }
    useEffect(() => console.log(count))
    // VÙNG MODAL
    const [openModal, setStateModal] = useState(false)
    return (
        <>
            <h1>Vietlot 6/45</h1>
            <BuyTicket
                changeBuyBall1={setbuyBall1}
                changeBuyBall2={setbuyBall2}
                changeBuyBall3={setbuyBall3}
                changeBuyBall4={setbuyBall4}
                changeBuyBall5={setbuyBall5}
                changeBuyBall6={setbuyBall6}
                setArr={pushArrBuyBall}
                buyBack={buyBackBall}
            ></BuyTicket>
            <hr />
            <Tickets arrTicket={arrBuyBall}></Tickets>

            <hr />
            <RandomTicket
                displayRandom={display}
                arrRandom={arrRandomBall}
                setArr={pushArrRandomBall}
                clickDoSo={compare2Arr}
            ></RandomTicket>
            <ModalResult
                open={openModal}
                countNumber={count}
                compareArr={compare}
                setModal={setStateModal}
                randomArr={arrRandomBall}
                ticketArr={arrBuyBall}
                >
            </ModalResult>
        </>
    )
}
export default Game1