import 'bootstrap/dist/css/bootstrap.min.css'
import './LuckyDraw.css'
import Content from './components/Content'
import { Container } from "@mui/material";
function App() {
  return (
    <div >
      <Container style={{maxWidth:600}}>
        <Content></Content>
      </Container>
    </div>
  );
}

export default App;
